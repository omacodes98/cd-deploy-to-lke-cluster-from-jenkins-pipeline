# Demo Project - CD Deploy to LKE cluster from jenkins Pipeline 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Create K8s cluster on LKE

* Install kubectl as Jenkins Plugin

* Adjust Jenkinsfile to use Plugin and deploy to LKE cluster

## Technologies Used 

* Kubernetes 

* Jenkins 

* Linode LKE 

* Docker 

* Linux 

## Steps 

Step 1: Create cluster on LKE 

[Create LKE cluster](/images/01_create_cluster_on_LKE.png)

Step 2: Download kubrconfig file when lke cluster is done being created 

[Download kubeconfig](/images/02_download_kubeconfig_file.png)

Step 3: Connect to LKE cluster by exporting KUBECONFIG and setting the downloaded lke kubeconfig as the value 

    export KUBECONFIG=~/Downloads/test-kubeconfig.yaml 

[Export kubeconfig](/images/03_connect_to_eks-cluster_by_exporting_kubeconfig_and_setting_value_as_the_recently_downloaded_kubeconfig_from_linode.png)

Step 4: Create upload lke kubeconfig as a secret file credential in multibranch pipeline scope 

[lke kubeconfig secret file](/images/04_create_credential_in_multibranch_pipeline_scope_which_is_secret_file_kind_and_upload_lke_kubeconfig_file_to_it.png)

[Created credential](/images/05_created_credential.png)

Step 5: Create Jenkinsfile which has a deploy stage that deploys on lke 

[Jenkinsfile deploy stage](/images/06_create_jenkinsfile_which_has_a_deploy_stage_that_deploy_on_lke.png)

Step 6: Install kubernetes cli plugins on jenkins to allow us connect to the cluster 

[Installing k8 cli](/images/06_install_kubernetes_cli_plugin_on_jenkins_to_allow_us_connect_kubectl_to_a_cluster.png)

Step 7: Push Jenkinsfile to gitlab and build pipeline

    git push 

[Push Jenkinsfile](/images/07_push_jenkinsfile_to_gitlab.png)

Step 8: Check if pipeline was successful

[Successful Pipeline](/images/08_successful_pipeline.png)

[Console logs](/images/09_console_log.png)

[Pods](/images/10_pod_successfully_running.png)

## Usage 

    kubectl create deployment nginx-deployment --image=nginx

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cd-deploy-to-lke-cluster-from-jenkins-pipeline.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/cd-deploy-to-lke-cluster-from-jenkins-pipeline

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.